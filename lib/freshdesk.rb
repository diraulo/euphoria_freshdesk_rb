require 'httparty'
require 'awesome_print'
require 'active_support/core_ext/hash'
require 'json'
require 'bundler'

Bundler.setup
require 'freshdesk'

TICKET_SOURCE = {:email => 1, :portal => 2, :phone => 3, :forum => 4, :twitter => 5, :facebook => 6, :chat => 7}
TICKET_STATUS = {:open => 2, :pending => 3, :resolved => 4, :closed => 5}
TICKET_PRIORITY = {:low => 1, :medium => 2, :high => 3, :urgent => 4}

class FreshdeskExt < Freshdesk
  include HTTParty

  base_uri 'http://sendalytic.freshdesk.com'

  def initialize(config = {})
    # ap config['base_uri']
    super(config['base_uri'], config['credentials']['email'], config['credentials']['password'])

    # Will use the following for requests with HTTParty
    # Cleaner solution would be to fork the freshdesk gem repository and extend
    # the existing functionalities
    # No time for it right now :-)
    # git_repo_url: https://github.com/dvliman/freshdesk-api

    @agents = config['agents']
    @options = {
      basic_auth: {
        username: config['credentials']['email'],
        password: config['credentials']['password']
      }
    }
  end

  ###
  # Create and Assign Ticket to agent from list of calls
  ###
  def create_tickets_from_calls(call_list, agent)
    # ap call_list
    unless call_list.none?
      call_list.each do |call|

        # Create ticket for the current call then assign it to a given agent
        query = {
          :subject      => "Support call",
          :description  => "Call from #{call['CallerID']}, Status: #{call['Status']}",
          :email        => "test@example.com",
          :responder_id => agent['id'],
          :priority     => TICKET_PRIORITY[:medium],
          :status       => TICKET_STATUS[:open],
          :source       => TICKET_SOURCE[:phone]
        }

        res = self.post_tickets(query)

        # ticket_id = self.get_new_ticket_id res
        # self.assign_tickets ticket_id, agent['id']
      end
    else
      puts "no call records were provided"
    end
  end

  def get_agents
    self.class.get("/agents.json", @options)
  end

  ###
  # Don't need the following methods for now since i can directly assign ticket
  # to agent when creating
  ###

  # def assign_tickets(ticket_id, agent)
  #   self.class.put("/helpdesk/tickets/#{ticket_id}/assign.json?responder_id=#{agent_id}", :basic_auth => auth)
  # end

  # def get_new_ticket_id(response)
  #   clean_s = response.gsub "<cc-email type=\"yaml\">--- \n:cc_emails: []\n\n:fwd_emails: []\n\n</cc-email>", ""
  #   res = Hash.from_xml(clean_s)
  #   res['helpdesk_ticket']['display_id']
  # end
end
