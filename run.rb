require 'yaml'

require_relative 'lib/euphoria'
require_relative 'lib/freshdesk'


class FileNotFound < StandardError; end

# Read configuration file
begin
  EUPHORIA_CONFIG = YAML.load_file('config.yml')['euphoria']
  FRESHDESK_CONFIG = YAML.load_file('config.yml')['freshdesk']
rescue
  raise FileNotFound, "Please make sure you have a copy of 'config.yml' in this folder and try again."
end

ap ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
ap "| Read call history from Euphoria and create tickets on Freshdesk"
ap "--------------------------------------------------------------------------"
puts

euphoria = Euphoria.new(EUPHORIA_CONFIG)
freshdesk = FreshdeskExt.new(FRESHDESK_CONFIG)

# Get and print list of agent to console
# ap freshdesk.get_agents

agents = FRESHDESK_CONFIG['agents']

agents.each do |agent|
  unless agent['id'] == 'unknown'
    ap agent
    # get call records for current agent
    ap ">> Getting call records for #{agent['name']}"
    calls_records = euphoria.get_call_details_records '50', agent['extension'], '300'

    # create tickets from returned calls
    freshdesk.create_tickets_from_calls calls_records[:calls], agent
  end
end
