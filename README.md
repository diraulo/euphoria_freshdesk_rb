# Euphoria - Freshdesk

### Setup

- Install `ruby >= v1.9.3`
- Install `bundler` for gem dependencies easy managment `gem install bundler`
- clone this repository
- `$ cd /path/to/project/directory`
- `$ bundle install`

### Config file
Make a copy of `config-sample.yml` and rename it to `config.yml` then make the following changes: <br>  

#### 1. Euphoria config

  Enter your Euphoria api key
```yaml
euphoria:
  ...
  credentials:
    Name: 'Everlytic'
    Auth: 'euphoria_api_key'

```

#### 2. Freshdesk Config
  Enter the user credentials then set up list of helpdesk agents.
  One way of getting the agent list is by querying the freshdesk api
  at this point there is no information about the agent extension
  from the api response, thus had to be hardcoded in `config.yml` file.

```yaml
freshdesk:
  base_uri: 'http://sendalytic.freshdesk.com/'
  credentials:
    email: 'user_email_fresdesk_support'
    password: 'password_fresdesk_support'
    api_key: 'freshdesk_user_api_key'
  agents:
    - name: 'Terence Ndhlovu'
      extension: '302'
      id: '9512221'
    - name: 'Jake Vermeulen'
      extension: '303'
      id: '12206467'
    - name: 'agent_name'
      extension: '308'
      id: 'unknown'
    - name: 'Phumelele Kubheka'
      extension: '309'
      id: '11836539'
    - name: 'agent_name'
      extension: '310'
      id: 'unknown'

```
### Run
Once everything is setup correctly run the following command from your terminal to execute the program:

```shell
$ ruby run.rb
```

<br>
### ToDo
- ~~Get call history from inboud call to support queue~~
- ~~create tickets in freshdesk `{priority: medium, status: open, source: phone}`~~
- ~~Assign created ticket to corresponding agent~~
- Paginate through euphoria API response (API doc does not provide much information on how to achieve that.)
- Avoid creation of duplicate tickets
- Give proper content while creating ticket (Ticket subject, description)
