require 'httparty'
require 'awesome_print'
require 'active_support/core_ext/hash'
require 'json'

class Euphoria
  include HTTParty

  base_uri "https://api.euphoria.co.za"

  def initialize(config = {})
    @api_path = config['api_path']
    @query = {
      :Tenant => config['credentials']
    }
  end

  def get_outbound_call_history(page_size, extension, startAt)
    query = build_query :name => 'GetOutboundCallingHistory', :pSize => page_size,
                        :start => startAt, :filter => extension
    response = self.class.get(@api_path, :body => query.to_xml(:root => 'XML'))
    ap response
  end

  def get_inbound_call_history(page_size, extension, startAt)
    query = build_query :name => 'GetInboundCallingHistory', :pSize => page_size,
                        :start => startAt, :filter => extension
    response = self.class.get(@api_path, :body => query.to_xml(:root => 'XML'))
    ap response
  end

  def get_call_details_records(page_size, extension, startAt)
    query = build_query :name => 'GetCallDetailRecords', :pSize => page_size,
                        :start => startAt, :filter => extension
    response = self.class.get(@api_path, :body => query.to_xml(:root => 'XML'))

    calls = []

    unless response.nil?
      response['XML']['CDR'].each do |call|
        # only return calls where destination was current extenstion
        if call['Destination'] == extension
          calls << call
        end
      end
    end

    {call_count: calls.length, calls: calls}
  end

  def build_query(options = {})
    query = @query
    query[:ActionName]   = options[:name]   unless options[:name].nil?
    query[:pageSize]     = options[:pSize]  unless options[:pSize].nil?
    query[:startAt]      = options[:start]  unless options[:start].nil?
    query[:filter]       = options[:filter] unless options[:filter].nil?
    query
  end
end
